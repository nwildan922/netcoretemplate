﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api
{
    public class ProcessStatus
    {
        public bool IsSucceed { get; set; }
        public string Token { get; set; }
    }
}
