﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Api.Controllers
{
    public abstract class BaseController : Controller
    {
        public override void OnActionExecuting(ActionExecutingContext context) 
        {
            var currentUser = context.HttpContext.User.Identity.Name;
            var headers = context.HttpContext.Request.Headers;
            var authorization = headers.Where(x => x.Key.Equals("Authorization")).FirstOrDefault();
            string token = "";
            token = authorization.Value;

            context.Result = Unauthorized();
            base.OnActionExecuting(context);
        }
    }
}
