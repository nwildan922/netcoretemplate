﻿using Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Api.Controllers
{
    public class AuthController : ControllerBase
    {
        private readonly TokenManagement _tokenManagement;
        private readonly ILdapAuthenticationService _ldapAuthService;

        public AuthController(IOptions<TokenManagement> tokenManagement, ILdapAuthenticationService ldapAuthService)
        {
            _tokenManagement = tokenManagement.Value;
            _ldapAuthService = ldapAuthService;
        }
        [HttpPost]
        [Route("api/Auth/UserLogin")]

        public ActionResult UserLogin([FromBody] User model)
        {
            string token = "";
            var process = new ProcessStatus() { 
                IsSucceed = false,
                Token = ""
            };
            var status = IsAuthenticated(model.Username,model.Password, out token);
            if (status) {
                process.IsSucceed = true;
                process.Token = token;
            }
            return Ok(process);
        }

        [HttpGet]
        [Authorize]
        [Route("api/Auth/GetCurrentUser")]
        public ActionResult GetCurrentUser() {
            string currentUser = HttpContext.User.Identity.Name;
            return Ok(currentUser);
        }


        public bool IsAuthenticated(string username,string password, out string token)
        {

            token = string.Empty;

            var user = _ldapAuthService.Login(username, password);

            if (user != null)
            {
                var claim = new[]
                {
                    new Claim(ClaimTypes.Name, username)
                };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenManagement.Secret));
                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var jwtToken = new JwtSecurityToken(
                    _tokenManagement.Issuer,
                    _tokenManagement.Audience,
                    claim,
                    expires: DateTime.Now.AddMinutes(_tokenManagement.AccessExpiration),
                    signingCredentials: credentials
                );
                token = new JwtSecurityTokenHandler().WriteToken(jwtToken);
                return true;
            }
            else 
            {
                return false;
            }

            

        }
    }
}
