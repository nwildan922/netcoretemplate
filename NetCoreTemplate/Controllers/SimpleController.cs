﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SimpleController : BaseController
    {
        // GET api/values/5
        [HttpGet]
        public ActionResult<string> Get() => "value";
    }
}
