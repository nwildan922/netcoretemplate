﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class FileUploadController : Controller
    {

        [HttpPost("Submit")]
        public async Task<IActionResult> Submit(SampleModel model)
        {
            try
            {

                string message = "";
                if (model.FormFile.Length > 0)
                {
                    // Get the object used to communicate with the server.
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://localhost/testwildan.txt");
                    request.Method = WebRequestMethods.Ftp.UploadFile;

                    // This example assumes the FTP site uses anonymous logon.
                    request.Credentials = new NetworkCredential("admin", "indocyber");

                    // Copy the contents of the file to the request stream.
                    byte[] fileContents;

                    using (var reader = new StreamReader(model.FormFile.OpenReadStream()))
                    {
                        string strStream = reader.ReadToEnd();
                        fileContents = Encoding.UTF8.GetBytes(strStream);
                    }


                    request.ContentLength = fileContents.Length;

                    using (Stream requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(fileContents, 0, fileContents.Length);
                    }

                    using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                    {
                        message = $"Upload File Complete, status {response.StatusDescription}";
                    }
                }
                //}

                return Ok(message);
            }
            catch (Exception ex)
            {

                return Ok(new { errorMessage = ex.Message });
            }
        }

        [HttpGet("DownloadFile")]
        public IActionResult DownloadFile(string name) 
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://localhost/" + name);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential("admin", "indocyber");

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            Stream responseStream = response.GetResponseStream();
            byte[] fileContents;

            using (var reader = new StreamReader(responseStream))
            {
                string strStream = reader.ReadToEnd();
                fileContents = Encoding.UTF8.GetBytes(strStream);
            }
            MemoryStream stream = new MemoryStream(fileContents);
            response.Close();
            //{ ".txt", "text/plain"},
            //{ ".pdf", "application/pdf"},
            //{ ".doc", "application/vnd.ms-word"},
            //{ ".docx", "application/vnd.ms-word"},
            //{ ".xls", "application/vnd.ms-excel"},
            //{
            //".xlsx", "application/vnd.openxmlformats
            //           officedocument.spreadsheetml.sheet"},
            //{ ".png", "image/png"},
            //{ ".jpg", "image/jpeg"},
            //{ ".jpeg", "image/jpeg"},
            //{ ".gif", "image/gif"},
            //{ ".csv", "text/csv"}
            return File(stream, "text/plain", name);
        }
    }    
}