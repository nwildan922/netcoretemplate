﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }
    }
    public class UserDetail : User
    {
        public string DisplayName { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
    }
}
