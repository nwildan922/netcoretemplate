﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models
{
    public class SampleModel
    {
        public int Id { get; set; }
        public int Name { get; set; }
        public IFormFile FormFile { get; set; }
    }
}
