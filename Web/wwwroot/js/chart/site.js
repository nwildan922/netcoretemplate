$(document).ready(function() {
    onload();
});
function onload() {
    getData();
    //generateChart();
    getListInstitutionName();
}

function getData() {
    $.ajax({
        type: "Get",
        url: "chart/GetHtml",
        dataType: "json",
        success: function(data) {
            $("#dynamicHeaderTable").append(data.headerTable);
            $("#dynamicDataTable").append(data.dataTable);
            $("table").rowspanizer({
                vertical_align: 'middle',
                columns: [0]

            });
        },
        error: function(){
            //alert("json not found");
        }
    });
}
function getListInstitutionName() {
    $.ajax({
        type: "Get",
        url: "chart/GetListInstitutionName",
        dataType: "json",
        success: function (data) {
            console.log("data list institution name", data);
            $('#ddlInstitutionName').empty();
            let html = '';
            for (var i = 0; i < data.length; i++) {
                html += `<option value='${ data[i] }'>${ data[i] }</option>`;
            }
            $('#ddlInstitutionName').append(html);
        },
        error: function () {
            //alert("json not found");
        }
    });
}
function generateChart() {
    let ctx = document.getElementById('myChart').getContext('2d');
    let chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',
    
        // The data for our dataset
        data: {
            labels: ["Januari","Februari","Maret"],
            datasets: [
                {
                    label: 'My First dataset',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: [ 3,0,3]
                },
                {
                    label: 'Second Dataset',
                    backgroundColor: 'rgb(255, 10, 10)',
                    borderColor: 'rgb(255, 10, 10)',
                    data: [3,1,4]
                }
            ]
        },
        options: {}
    });
}
function generateCustomChart(institutionName) {
    $.ajax({
        type: "Get",
        url: `chart/GetDataChart?institutionName=${institutionName}`,
        dataType: "json",
        success: function (data) {

            let ctx = document.getElementById('myChart').getContext('2d');
            let chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'bar',

                // The data for our dataset
                data: {
                    labels: data.labels,
                    datasets: data.datasets
                },
                options: {}
            });
        },
        error: function () {
            alert("json not found");
        }
    });
    
}
function onChangeDdlChart1() {
    let temp = $('#ddlInstitutionName').val();
    generateCustomChart(temp);
    //generateChart();
}
function getDummyData() {
    return {
        labels:[
            'My FirstDataSet','SecondDataSet'
        ],
        datasets: [
            {
                label: 'My First dataset',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: [2, 20]
            },
            {
                label: 'Second Dataset',
                backgroundColor: 'rgb(255, 10, 10)',
                borderColor: 'rgb(255, 10, 10)',
                data: [1, 10]
            }
        ]
    };
}