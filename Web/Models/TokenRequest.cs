﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class TokenRequest
    {
        [Required]
        [JsonProperty("username")]
        public string Username { get; set; }


        [Required]
        [JsonProperty("password")]
        public string Password { get; set; }
        [Required]
        [JsonProperty("isSuperAdmin")]
        public bool IsSuperAdmin { get; set; }

    }
    public interface IAuthenticateService
    {
        bool IsAuthenticated(TokenRequest request, out string token);
    }
    public class TokenAuthenticationService : IAuthenticateService
    {
        public bool IsAuthenticated(TokenRequest request, out string token)
        {
            throw new System.NotImplementedException();
        }
    }
    
}
