﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class UserDetail : User
    {
        public string DisplayName { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
    }
}
