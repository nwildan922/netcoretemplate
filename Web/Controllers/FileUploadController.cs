﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class FileUploadController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("Submit")]
        //public async Task<IActionResult> Submit(List<IFormFile> files)
        public async Task<IActionResult> Submit(SampleModel model)
        {
            try
            {
                //long size = files.Sum(f => f.Length);
                string message = "";
                //var filePaths = new List<string>();
                //foreach (var formFile in files)
                //{
                    //if (formFile.Length > 0)
                    if (model.FormFile.Length > 0)
                    {
                        // Get the object used to communicate with the server.
                        FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://localhost/testwildan.txt");
                        request.Method = WebRequestMethods.Ftp.UploadFile;

                        // This example assumes the FTP site uses anonymous logon.
                        request.Credentials = new NetworkCredential("admin", "indocyber");

                        // Copy the contents of the file to the request stream.
                        byte[] fileContents;

                        using (var reader = new StreamReader(model.FormFile.OpenReadStream()))
                        {
                            string strStream = reader.ReadToEnd();
                            fileContents = Encoding.UTF8.GetBytes(strStream);
                        }


                        request.ContentLength = fileContents.Length;

                        using (Stream requestStream = request.GetRequestStream())
                        {
                            requestStream.Write(fileContents, 0, fileContents.Length);
                        }

                        using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                        {
                            message = $"Upload File Complete, status {response.StatusDescription}";
                        }
                    }
                //}

                return Ok(message);
            }
            catch (Exception ex)
            {

                return Ok(new { errorMessage = ex.Message });
            }
            
        }
    }
}