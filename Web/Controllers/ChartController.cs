﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    public class ChartController : Controller
    {
        private readonly List<DataReport> _data;
        private const int MaxPerField = 3; 
        public ChartController()
        {
            _data = new List<DataReport>();
            _data.Add(new DataReport { Tahun = 2019, Semester = "S01", Nop = 1,Noj=3, Edulevel = "Profession", Institution="ITB", Major="Software" });
            _data.Add(new DataReport { Tahun = 2019, Semester = "S01", Nop = 1, Noj = 3, Edulevel = "Profession", Institution = "UNPAD", Major = "Software" });
            _data.Add(new DataReport { Tahun = 2019, Semester = "S01", Nop = 1, Noj = 3, Edulevel = "S1", Institution = "UNPAR", Major = "Business Analyst" });
            _data.Add(new DataReport { Tahun = 2019, Semester = "S02", Nop = 1, Noj = 3, Edulevel = "S1", Institution = "UNPAR", Major = "Business Analyst" });
            _data.Add(new DataReport { Tahun = 2019, Semester = "S02", Nop = 1, Noj = 3, Edulevel = "SMP", Institution = "Arkham Asylum", Major = "System Information" });
            _data.Add(new DataReport { Tahun = 2020, Semester = "S01", Nop = 1, Noj = 3, Edulevel = "SD", Institution = "Arkham Asylum", Major = "System Information" });
            _data.Add(new DataReport { Tahun = 2020, Semester = "S02", Nop = 1, Noj = 3, Edulevel = "SD", Institution = "Arkham Asylum", Major = "System Information" });
            _data.Add(new DataReport { Tahun = 2020, Semester = "S02", Nop = 1, Noj = 3, Edulevel = "SD", Institution = "Arkham Asylum", Major = "System 2" });
            _data.Add(new DataReport { Tahun = 2021, Semester = "S01", Nop = 1, Noj = 3, Edulevel = "STM", Institution = "UNPAR", Major = "Major Baru" });
        }
        public IActionResult Index()
        {
            return View();
        }       
        [AllowAnonymous]
        public IActionResult GetHtml() 
        {
            var listYear = ChartHelper.GetListDistinctYear(_data);
            var listSemester = ChartHelper.GetListMapYearSemester(_data);
            var tempHeaderTable = ChartHelper.GenerateHeaderTableHtml(listYear, listSemester, MaxPerField) ;
            var tempDetailTable = ChartHelper.GenerateDataTableHtml(listYear,listSemester,_data, MaxPerField);
            var data = new { headerTable =  tempHeaderTable, dataTable = tempDetailTable};
            return Ok(data);
        }

        [AllowAnonymous]
        public IActionResult GetListInstitutionName()
        {
            var data = ChartHelper.GetListDistinctInstitutionName(_data);
            return Ok(data);
        }

        [AllowAnonymous]
        public IActionResult GetDataChart(string institutionName)
        {
            var data = ChartHelper.GetRealDataChart(institutionName,_data);
            return Ok(data);
        }
    }
    class Chart 
    {
        public List<string> Labels { get; set; }
        public List<ChartDataSet> Datasets { get; set; }
    }

    class ChartDataSet 
    {
        public string Label { get; set; }
        public string BackgroundColor { get; set; }
        public string BorderColor { get; set; }
        public int[] Data { get; set; }
    }

    public class DataReport 
    {
        public int Tahun { get; set; }
        public string Semester { get; set; }
        public int Nop { get; set; }
        public int Noj { get; set; }
        public string Edulevel { get; set; }
        public string Institution { get; set; }
        public string Major { get; set; }
    }
    class Semesters 
    {
        public string Semester { get; set; }
        public int Nop { get; set; }
        public int Noj { get; set; }
    }
    class InstitutionNames 
    {
        public string InstitutionName { get; set; }
    }
    class MajorGroup 
    {
        public string MajorGroupName { get; set; }
        public List<InstitutionNames> ListInstitutionName { get; set; }
    }
    class MapYearSemester 
    {
        public int Tahun { get; set; }
        public List<string> Semesters { get; set; }
    }
    class MapEducationLevelMajorGroup
    {
        public string educationLevel { get; set; }
        public List<MajorGroup> ListMajorGroup { get; set; }
    }
    class MapInstitutionNameMajorGroupData 
    {
        public string InstitutionName { get; set; }
        public string MajorGroup { get; set; }
        public int[] Data { get; set; }
    }
    static class ChartHelper
    {
        public static Chart GetDataChart()
        {
            return GetDummyDataChart();
        }
        public static string[] GetColor() {
            return new string[] { "rgb(50, 50, 50)", "rgb(200, 200, 200)", "rgb(100, 100, 100)" };
        }
        public static Chart GetRealDataChart(string institutionName,IEnumerable<DataReport> data)
        {
            //collect labels { majorgroup berdasarkan institution name }
            var listMajorGroupByInsitutionName = (from x in data where x.Institution == institutionName select x.Major).Distinct().ToList();
            var datasets = new List<ChartDataSet>();
            var listSemesterEachYear = GetListMapYearSemester(data);
            var listMapInstitutionNameMajorGroupData = new List<MapInstitutionNameMajorGroupData>();
            int sequence = 0;
            var listLabels = new List<string>();

            foreach (var yearSemester in listSemesterEachYear)
            {
                foreach (var semester in yearSemester.Semesters)
                {
                    string temp = $"{yearSemester.Tahun} - {semester}";
                    listLabels.Add(temp);
                }
            }

            //code baru
            foreach (var majorGroup in listMajorGroupByInsitutionName)
            {
                var tempDataSet = new ChartDataSet();
                var listData = new List<int>();
                foreach (var yearSemester in listSemesterEachYear)
                {
                    foreach (var currentSemester in yearSemester.Semesters)
                    {
                        var temp = data.Where(x => x.Tahun.Equals(yearSemester.Tahun) && x.Semester.Equals(currentSemester) && x.Major.Equals(majorGroup) && x.Institution.Equals(institutionName)).FirstOrDefault();
                        if (temp != null)
                            listData.Add(temp.Noj / temp.Nop);
                        else
                            listData.Add(0);
                    }
                }
                tempDataSet.Label = majorGroup;
                tempDataSet.BackgroundColor = GetColor()[sequence];
                tempDataSet.BorderColor = GetColor()[sequence];
                tempDataSet.Data = listData.ToArray();
                datasets.Add(tempDataSet);
                sequence++;
            }

            return new Chart { Labels = listLabels, Datasets = datasets };
        }
        public static Chart GetDummyDataChart()
        {
            var labels = new List<string>();
            labels.Add("My First Data Set");
            labels.Add("My Second Data Set");
            labels.Add("My Third Data Set");

            var datasets = new List<ChartDataSet>();
            datasets.Add(new ChartDataSet { Label = "My First Data Set", BackgroundColor = "rgb(255, 99, 132)", BorderColor = "rgb(255, 99, 132)", Data = new int[] { 2, 5 } });
            datasets.Add(new ChartDataSet { Label = "My Second Data Set", BackgroundColor = "rgb(255, 10, 10)", BorderColor = "rgb(255, 10, 10)", Data = new int[] { 1, 3 } });
            datasets.Add(new ChartDataSet { Label = "My Third Data Set", BackgroundColor = "rgb(100, 10, 10)", BorderColor = "rgb(255, 10, 10)", Data = new int[] { 2, 3 } });
            return new Chart { Labels = labels, Datasets = datasets };
        }

        public static List<int> GetListDistinctYear(IEnumerable<DataReport> data)
        {
            return (from x in data select x.Tahun).Distinct().ToList();
        }
        public static List<string> GetListDistinctInstitutionName(IEnumerable<DataReport> data)
        {
            return (from x in data select x.Institution).Distinct().ToList();
        }
        public static List<string> GetSemesterByYear(int year, IEnumerable<DataReport> data)
        {
            return (from x in data where x.Tahun == year select x.Semester).Distinct().ToList();
        }
        public static List<MapYearSemester> GetListMapYearSemester(IEnumerable<DataReport> data)
        {
            var listYear = GetListDistinctYear(data);
            var listSemesterEachYear = new List<MapYearSemester>();
            foreach (var currentYear in listYear)
            {
                var tempCurrentYearSemesters = GetSemesterByYear(currentYear, data);
                listSemesterEachYear.Add(new MapYearSemester { Tahun = currentYear, Semesters = tempCurrentYearSemesters });
            }
            return listSemesterEachYear;
        }
        public static IEnumerable<string> GetDistinctEducationLevel(List<DataReport> data)
        {
            return (from x in data select x.Edulevel).Distinct();
        }
        public static List<MapEducationLevelMajorGroup> GetMapEducationLevelMajorGroups(List<DataReport> data) 
        {
            var vm = new List<MapEducationLevelMajorGroup>();
            var listEducationLevel = GetDistinctEducationLevel(data);
            foreach (var educationLevel in listEducationLevel)
            {
                var currentMajorGroup = new List<MajorGroup>();
                var tempStr = new List<string>();
                foreach (var item in data)
                {
                    if (item.Edulevel == educationLevel) {
                        tempStr.Add(item.Major);
                    }
                }
                var distinctStr = tempStr.Distinct();
                foreach (var item in distinctStr)
                {
                    currentMajorGroup.Add(new MajorGroup { MajorGroupName = item });
                }
                vm.Add(new MapEducationLevelMajorGroup { educationLevel = educationLevel, ListMajorGroup = currentMajorGroup });
            }
            return vm;
        }
        public static List<string> GetInstitutionNameByEducationLevelAndMajorGroupName(string educationLevel, string majorGroupName, List<DataReport> data) 
        {
            var vm = (from x in data where x.Edulevel == educationLevel && x.Major == majorGroupName select x.Institution).Distinct().ToList();
            return vm;
        }
        public static string GenerateHeaderTableHtml(List<int> listYear, List<MapYearSemester> listSemesters, int maxFieldPerValue) 
        {
            string html = "";
            string tempHeaderYear = "";
            string headerYear = "";
            int totalColspansEachYear = 0;
            for (var i = 0; i < listYear.Count; i++)
            {
                totalColspansEachYear = listSemesters[i].Semesters.Count * maxFieldPerValue;
                tempHeaderYear += $"<td colspan = { totalColspansEachYear }> { listYear[i] } </td>";
            }
            headerYear = $@"<tr>
                            <td rowspan='3'> Education Level </td>     
                            <td rowspan='3'> Major Group </td>          
                            <td rowspan='3'> Institution Name </td>
                            { tempHeaderYear }
                        </tr>";
            string tempHeaderSemester = "";
            for (int i = 0; i < listSemesters.Count; i++)
            {
                for (int j = 0; j < listSemesters[i].Semesters.Count; j++)
                {
                    tempHeaderSemester += $"<td colspan='{ maxFieldPerValue }'> { listSemesters[i].Semesters[j] }</td>";
                }
            }
            string headerSemester = $"<tr>{ tempHeaderSemester }</tr>";

            string tempDetail = "";
            for (var i = 0; i < listYear.Count; i++)
            {
                var temp = listSemesters.FirstOrDefault(x => x.Tahun.Equals(listYear[i]));

                var dataCurrentYear = temp != null ? temp.Tahun : 0;
                int totalSemesterinCurrentYear = temp.Semesters.Count;
                for (var j = 0; j < totalSemesterinCurrentYear; j++)
                {
                    tempDetail += "<td>Number Of Placement</td><td>Number Of Joining</td><td>Acceptance Index</td>";
                }
            }
            string detail = $"<tr> { tempDetail }</tr>";
            html = $"{ headerYear}{ headerSemester}{ detail}";
            return html;
        }
        public static string GenerateDataTableHtml(List<int> listYear, List<MapYearSemester> listSemesters,List<DataReport> data, int maxFieldPerValue) 
        {
            string htmlString = "";
            string dynamicDetail = "";
            var currentInstitutionName = new List<string>();
            string currentMapEducationLevel = "";
            string currentMajorGroup = "";
            var listEducationLevelMajorGroups = GetMapEducationLevelMajorGroups(data);
            //map education level, major group with education level
            foreach (var educationLevelMajorGroup in listEducationLevelMajorGroups)
            {
                currentMapEducationLevel = educationLevelMajorGroup.educationLevel;
                var currentEducationLevel = educationLevelMajorGroup.educationLevel;
                string totalHtml = "";
                foreach (var majorGroup in educationLevelMajorGroup.ListMajorGroup)
                {
                    currentMajorGroup = majorGroup.MajorGroupName;
                    currentInstitutionName = GetInstitutionNameByEducationLevelAndMajorGroupName(currentEducationLevel, majorGroup.MajorGroupName, data);
                    foreach (var institutionName in currentInstitutionName)
                    {
                        //dynamic detail
                        string tempDetail = "";
                        
                        foreach (var year in listYear)
                        {
                            var tempListSemester = (from x in listSemesters where x.Tahun == year select x.Semesters).ToList();
                            int totalSemesterCurrentYear = tempListSemester[0].Count;
                            for (int i = 0; i < totalSemesterCurrentYear; i++)
                            {
                                var temp = data.Where(x => x.Edulevel.Equals(currentEducationLevel) && x.Major.Equals(majorGroup.MajorGroupName) && x.Institution.Equals(institutionName) && x.Semester.Equals(tempListSemester[0][i]) && x.Tahun.Equals(year)).FirstOrDefault();
                                int acceptanceIndex = temp == null ? 0 : temp.Noj / temp.Nop;
                                string tempHtml = temp == null ? $"<td>0</td><td>0</td><td>{ acceptanceIndex }</td>" : $"<td>{ temp.Nop }</td><td>{ temp.Noj }</td><td>{ acceptanceIndex }</td>";
                                tempDetail += tempHtml;
                            }
                        }

                        dynamicDetail = tempDetail;

                        //create html
                        htmlString += $@"<tr><td>{ currentMapEducationLevel }</td><td>{ currentMajorGroup }</td><td>{ institutionName }</td>{ dynamicDetail }</tr>";
                    }
                }
                totalHtml = "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                htmlString += $@"<tr><td></td><td>Total</td><td></td>{ totalHtml }</tr>";
            }
            return htmlString;
        }
    }
}